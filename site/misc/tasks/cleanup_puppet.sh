#!/bin/bash

PIDS=$(ps -ef | grep puppet | grep -v grep | grep -v clean | awk '{print $2}')
DIRS=" /opt/puppetlabs/server /var/log/puppetlabs/ /etc/puppetlabs /tmp/puppet-enterprise /tmp/puppet-enterprise-2* \
 /var/cache/puppetlabs /var/puppetlabs /var/lib/yum/repos/x86_64/7/puppet_enterprise /var/cache/yum/x86_64/7/pe_repo/ \
 /etc/yum.repos.d/pe_repo.repo /etc/yum.repos.d/puppet_enterprise.repo /opt/puppetlabs/puppet/cache/state/agent_catalog_run.lock \
 /var/cache/yum/ "
RPMS=$(rpm -qa | grep -e '^pe-' -e 'puppet-agent')

if [[ $PIDS ]]; then 
  kill -9 $PIDS
  echo "Killed PIDs: $PIDS"
fi

if [[ "${RPMS}x" != 'x' ]]; then 
  rpm -e $RPMS
else
  echo "No RPMS to remove: $RPMS"
fi

for DIR in $DIRS ; do 
  if [[ -e $DIR ]]; then
    echo "Removing $DIR"
    rm -rf $DIR
  else
    echo "$DIR: does not exist"
  fi
done



#!/opt/puppetlabs/bin/puppet apply
function param($name) { inline_template("<%= ENV['PT_${name}'] %>") }

$add_data=param('add_data_to_group')
$puppetdb_database_host=param('puppetdb_database_host')

if $add_data {
  node_group { 'PE Compile Masters':
    ensure               => 'present',
    classes              => {
      'profile::compile_master' => { }
    },
    data                 => {
      'puppet_enterprise::profile::puppetdb' => {
        'database_host' => $puppetdb_database_host,
      }
    },
    rule                 => ['and',
    ['=',
      ['trusted', 'extensions', 'pp_role'],
      'pe_xl::compile_master']],
  }
} else {
  node_group { 'PE Compile Masters':
    ensure               => 'present',
    data                 => { },
    classes              => {
      'profile::compile_master' => { }
    },
    parent               => 'PE Master',
    rule                 => ['and',
    ['=',
      ['trusted', 'extensions', 'pp_role'],
      'pe_xl::compile_master']],
  }
}

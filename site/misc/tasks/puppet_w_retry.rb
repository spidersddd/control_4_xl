#!/opt/puppetlabs/puppet/bin/ruby
#
# Puppet Task to clean a node's certificate
# This can only be run against the Puppet Master.
#
# Parameters:
#   * agent_certnames - A comma-separated list of agent certificates to clean/remove.
#
require 'puppet'
require 'open3'

Puppet.initialize_settings

def agent_run(command)
  stdout, stderr, status = Open3.capture3(command)
  {
    stdout: stdout.strip,
    stderr: stderr.strip,
    exit_code: status.exitstatus,
  }
end

results = {}
command = '/opt/puppetlabs/bin/puppet agent \
  --onetime \
  --verbose \
  --no-daemonize \
  --no-usecacheonfailure \
  --no-splay \
  --no-use_cached_catalog'

begin
  retries ||= 0
  output = agent_run(command)
  results[:result] = if output[:exit_code].zero?
                              "Puppet Run Completed Successfully with #{retries} retry"
                            else
                              output
                            end
  raise output
rescue
  retry if (output[:exit_code] != 0) and ((retries += 1) < 2)
end


puts results.to_json

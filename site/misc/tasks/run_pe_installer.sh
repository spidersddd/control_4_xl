#!/bin/bash

set -e

if [[ "${PT_pe_conf}x" != 'x' || "${PT_installer_path}x" != 'x' ]]; then
  exec $PT_installer_path -c $PT_pe_conf
fi


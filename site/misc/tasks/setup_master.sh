#!/bin/bash
        
        cat > /home/centos/.ssh/GPG-KEY-frankenbuilder.pub <<-EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)

mQENBFf+Y8EBCADJIWF6uRoJRuaEkulMhGje7DyUI/QTbG+MP5QuQnBkhnlMh9+Y
X8V+Hh99fyIWzt4QP9K9LrSINWXd4RFyY60lNusGgapXykKy5lDFO0rBO5XutENS
vgb++DJUZuV7IM3Veo20G0bzcurUxqJjaz0MR2uznd0faVscjn8PxELxCqJU9AGK
l9fyQ6fIz4hk4SHabL6YhZHwVXciXbQDKNpM7cuHW4h6atvWoKP3/2tUrYu33hIc
6Mc03ujLWGgIv3llSPf+/kHZxpSIdHHNA7EgcQMFdlsG9QcVdyuowjMAPwPngD8T
PinpNnUcGBL8f/RFgIovqW5WiIiy4ITr8u5vABEBAAG0QUZyYW5rZW5idWlsZGVy
IFNpZ25pbmcgS2V5IDx0ZWFtLW9yZ2FuaXphdGlvbmFsLXNjYWxlQHB1cHBldC5j
b20+iQE5BBMBAgAjBQJX/mPBAhsDBwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AA
CgkQbDEiDruw6TuREwf7BuP9394IjRcf0oIc91eGpbGgDjqle8CRs4W88o8FPdPY
ejP/xo/Z+7dO7htrss26knpeRBtK2oOc3JNg3nXO732IwL6gQCyrU8dKGU7kuPrK
JQWa9eQY2ZJ9gKlBDhaTb7ehIC8MUpkTPktvcyl3880DXJ6IuzMBCXapnzpFjtI/
CDOBFhnaf9FVZWbW9YgRHRomW9EaeBzPN1hPj4EpbflR6Dj08f/zOFmtPH8N93an
em+JyQP3EjZlq5adPPKIdFy92xx5BY++z5Lev3pHmAZS0ZBHhsDbdO76p9w6do2m
Mc7dI7dhAxcxdSMgVmarSo4VNfCd1bcvvq0gXB9MybkBDQRX/mPBAQgAvdeAQ2Vz
jC0rsdf99sSna7i89br7MkdkU/C3pA/hkV2tVVCVHOjUqjVDHGK4byaxN95wUkmh
nwrBQg0D3HYBI+vfgpzhFphWe/rjY4eavyIOOD2lrqWIYBFy4d7SqUcN7sWK5Nca
uWa5QeUr8cqFKyxB64MjPJHweCubLGvQdQSc9ZwmI4eZ3QGZX+IPkuxLbc12ebII
hHbl9i4jNYv8+IP5edfUT8Qdr2DdZ503hWYEFfVGNtkU5f4ZyPFGYpS6UuP2+uL0
c9kUlpcjJgoSShDEZOb4GKjxhJ7wktSeyCgXhL/vTWHpfr8jh+/elAPkb+VDgrGJ
X6D5LGLvqZ/GxwARAQABiQEfBBgBAgAJBQJX/mPBAhsMAAoJEGwxIg67sOk7/lUH
/2//G6kckySaLe1cLZtmpLa3PBsLYE3UQMfdt6GtoH2AZevTWMKh5f6RnRmNpP7m
mXRzb+feCGBW2WwUffe2W5TzCEgyo3MZe39FHwvUsw3iXLMg6j1tQXb/5PQoLWw+
EuRTUAcu2btALC/Gq0YGSumL/BNZ4MlFewg1WYSQlyxibd36GGJXVzzuxGGjfhmr
hGNjiW4F2FdgmX0Z7KEVFSWcM8B3QUtDyUEibk8+NMiG74buEO+mGUAz80ykwHMx
f6AkJg307WHg5apR907VSHAol0j29nVqrTqwMdgd354n76eI64/Jx5gAfTizjSex
t3YYWb2nnrAV6sIbq0dkCX0=
=/fnd
-----END PGP PUBLIC KEY BLOCK-----
EOF
	cat > /home/centos/.ssh/id_rsa-acceptance <<-EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzOvWRfpOXxsxd1S7Nbihz/yvN+yCda3dXjL0AmGaoa5rb7wE
3ivuTdAqtDvwdz5XnEjm7EMF34XhJa4v1riR5J662fRDJIJBekFMD7no/zSzM58Z
K6U4mXSeIODG9MUCsxfgCm98wPlnjuOLe3Ta3p2dSrtT0smnVEx+3P7+Jfs3GhIG
9bLP7tJQSVcdUnlRnPTSeH3fn6c6+pOcaYO2qmOkZ8myxmcP0VSY3cA8K1gl24b6
8K4Kv+B8SOHgJAsoX2zjWbnEbhsfZD3CmG/44+BCVrSK/cQ8kctKR0FEcX+Z6elE
yYs4LKeTAqhMpuP1vvbsqLgiDx6NOwPzb6PCzQIDAQABAoIBAAdwTC/wHSYwq4QE
bGNP13fgWU/zYuz4qb+ApADuIBncZgQBVD/oYlcqxlISlvENL0r6Dn/nEvCBQKII
h64ItysQZAIDbuiFXfEdK7Cqc/3HoPWvwc8myT4w0IirAC3kdOuhYz2dvG6Y3xx9
wBN0M3m/SPVshWdycIOL31MQYpNoUXAf90aL9rrIVJJYSEDvp7wYRStxM9W1DcEK
e+nWyvHZRGCbkWVWXZiWs+AfCbEgZVWTyXO3kQ0tiegsEPJ6NyXc/W5q1EVuPzpx
Z3tLcfHVn2bVDFXfNImZk9a5psYm46MFfMwcO+f/BxC/nmKxKhdjwV85vSiMw/kM
x3YkIrUCgYEA+Y5yuIWn4TNSDLcTvqfgbpF4w7aS3waZdeZHYcT+Ymw2Td0I8G2b
WyDr6pQzHSIMOdo72p8sdea63T7uHvQ62Qb7ba+7ZOKCLGfoixNaGyQo/q7/gJn2
0TYAn5g6A8fmKJ6Nn5jqN/GBVywkYqOZzgb5rKdWRYhOkXsHah2deH8CgYEA0jZa
ZN9kN7fV/VaOanrUNuYJUZz34rFHE0zfw87zOGkylibl/cG9SyFdjBY7QGCOsnAC
YUJwY5dVtodRk4CcKQdKsF/S003GSfdsJUJEwfS0qAEwV3Ip6xxKWbrP/u9dopar
niGPdX/Pfq/PcNACS9DBcguX1uZgcex0knnGfrMCgYEAxB7eqYTgWmsqoDOAoOTt
y/cnISfqZLua/rzqfDuEb3T7eFWtIMYCwastvDybrtmDIjQPtdBB8KZTVGyQVmM5
nDWQgrMIYlBgXOWMK0AaIBpa3UxKH5bgEaqO8t1/Ollu151DT5ms+Cojq45VSe+o
f/GbZcRcWGi21j4JpauAhaECgYAV2yze3BMxb93B72PzUj4KTzSc1bM4ULJRIWcK
ltPXA6kMz5KOJbymuRte21jhwqhTiS3WKvBAipWQ2tTyfya9ZVKwr+XdrnJ1clI+
NXuspuK5rRX3oZkWJuTcG35QxVZ49y10zYx67QUH6QOqeRYkB9p1Hxt0bpOMhMdC
C4NF3QKBgBgTgAohZhxXFnNzCri7R9OknQeOwbh6P19KsRtG8FG8kCV7470AlCgD
m5WSYQoWVcXsz+r7HwP+C7a9mNtPz8tKoPoDUcyKqHsPr2XcPrDKj9aOghjwOYQv
VYR6BfVt+CCbiBL5xj0hMVy+bfEqsjCjIfqoDao9QhzWg/t4i/py
-----END RSA PRIVATE KEY-----
	EOF

rpm --import /home/centos/.ssh/GPG-KEY-frankenbuilder.pub
chmod 700 /home/centos/.ssh/id_rsa-acceptance

/opt/puppetlabs/puppet/bin/puppet apply -e 'package { git: ensure => installed, }'

mkdir -p /opt/code
chmod 777 /opt/code

         cat > /home/centos/.ssh/config <<-EOF
Host gitlab.com
    Hostname gitlab.com
    IdentityFile /home/centos/.ssh/id_rsa-acceptance
    IdentitiesOnly yes
Host github.com
    Hostname github.com
    IdentityFile /home/centos/.ssh/id_rsa-acceptance
    IdentitiesOnly yes
	EOF

chmod 700 /home/centos/.ssh/id_rsa-acceptance
chown centos.centos /home/centos/.ssh/id_rsa-acceptance
chown centos.centos /home/centos/.ssh/config
chmod 700 /home/centos/.ssh/config
sudo -u centos '/bin/ssh -oStrictHostKeyChecking=no gitlab.com > /dev/null 2>&1'
sudo -u centos git clone git@gitlab.com:spidersddd/control_4_xl.git /opt/code/control_repo


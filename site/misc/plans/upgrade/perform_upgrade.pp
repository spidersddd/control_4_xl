plan misc::upgrade::perform_upgrade (
  String[1]           $version = '2018.1.2',
  String[1]           $console_password,
  Hash                $r10k_sources = { },

  String[1]           $primary_master_host,
  String[1]           $puppetdb_database_host,
  Array[String[1]]    $compile_master_hosts = [ ],
  Array[String[1]]    $dns_alt_names = [ ],

  Optional[String[1]] $primary_master_replica_host = undef,
  Optional[String[1]] $puppetdb_database_replica_host = undef,

  Optional[String[1]] $load_balancer_host = undef,
  Optional[String[1]] $token_file = undef,

  String[1]           $stagingdir = '/tmp',
  Optional[String[1]] $pe_tarball_location = "https://s3.amazonaws.com/pe-builds/released/${version}/puppet-enterprise-${version}-el-7-x86_64.tar.gz",
) {

  # Set variables and groups of nodes
  $all_hosts_upgrade = [
    $primary_master_host,
    $puppetdb_database_host,
    $primary_master_replica_host,
    $puppetdb_database_replica_host,
  ].pe_xl::flatten_compact()

  $dns_alt_names_csv = $dns_alt_names.reduce |$csv, $x| { "${csv},${x}" }

  # Get last name for dns_alt_names and assign to balancer
  if $compile_master_hosts and $dns_alt_names {
    $balancer = $dns_alt_names[-1]
  }

  # Download the PE tarball and send it to the nodes that need it
  $pe_tarball_name     = "puppet-enterprise-${version}-el-7-x86_64.tar.gz"
  $local_tarball_path  = "${stagingdir}/${pe_tarball_name}"
  $upload_tarball_path = "/tmp/${pe_tarball_name}"

  # Build replica enable command
  $enable_replica_cmd = 'env PATH=/opt/puppetlabs/bin:$PATH /opt/puppetlabs/bin/puppet infrastructure enable replica '
  if $token_file {
    $token_options = "--token-file=${token_file}"
  } else {
    $token_options =  ''
  }
  $enable_options_1 = "$token_options \
    --pcp-brokers=${primary_master_host}:8142 --agent-server-urls=${balancer}:8140 \
    --infra-agent-server-urls=${primary_master_host}:8140  \
    --infra-pcp-brokers=${primary_master_host}:8142,${primary_master_replica_host}:8142 \
    --topology=mono-with-compile --classifier-termini=${primary_master_host}:4433 \
    --puppetdb-termini=${balancer}:8081 --skip-agent-config --yes "

  # Run puppet to change any configs needed to point to primary_master_host
  $all_hosts_upgrade.each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }

  # Run the enable command to point all infrastecture at primary_master_host
  run_task(misc::enable_replica, $primary_master_host,
    primary_master_replica => $primary_master_replica_host,
    command_options        => $enable_options_1,
  )
  
  # Run puppet to change any configs needed to point replica
  [$primary_master_host,$puppetdb_database_host].each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }

  # Run puppet to change any configs needed to point to primary_master_host
  $all_hosts_upgrade.each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }

  # Get the primary master replica set upgrade done.
  [$primary_master_replica_host,$puppetdb_database_replica_host].each |$host| {
    without_default_logging() || {
      notice("Starting: task pe_xl::pe_install on ${host}")
      run_task('pe_xl::pe_install', $host,
        _catch_errors         => true,
        tarball               => $upload_tarball_path,
        peconf                => '/tmp/pe.conf',
      )
      notice("Finished: task pe_xl::pe_install on ${host}")
    }
  }

  # Run puppet to change any configs needed to point replica
  [$primary_master_replica_host,$puppetdb_database_replica_host].each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }


  # Run puppet to change any configs needed to point to primary_master_host
  $all_hosts_upgrade.each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }

  $enable_options_on_replica = "$token_options \
    --pcp-brokers=${primary_master_host}:8142 --agent-server-urls=${balancer}:8140 \
    --infra-agent-server-urls=${primary_master_replica_host}:8140  \
    --infra-pcp-brokers=${primary_master_host}:8142,${primary_master_replica_host}:8142 \
    --topology=mono-with-compile --classifier-termini=${primary_master_replica_host}:4433 \
    --puppetdb-termini=${balancer}:8081 --skip-agent-config --yes"

  # Run the enable command to point all infrastecture at primary_master_host
  run_task(misc::enable_replica, $primary_master_host,
    primary_master_replica => $primary_master_replica_host,
    command_options        => $enable_options_on_replica,
  )

  # Run puppet to change any configs needed to point to primary_master_host
  $all_hosts_upgrade.each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }

  # Get the primary master set upgrade done.
  [$primary_master_host,$puppetdb_database_host].each |$host| {
    without_default_logging() || {
      notice("Starting: task pe_xl::pe_install on ${host}")
      run_task('pe_xl::pe_install', $host,
        _catch_errors         => true,
        tarball               => $upload_tarball_path,
        peconf                => '/tmp/pe.conf',
      )
      notice("Finished: task pe_xl::pe_install on ${host}")
    }
  }

}
#  #  # Create csr_attributes.yaml files for the nodes that need them
#  #  run_task('pe_xl::mkdir_p_file', $primary_master_host,
#  #    path    => '/etc/puppetlabs/puppet/csr_attributes.yaml',
#  #    content => @("HEREDOC"),
#  #      ---
#  #      extension_requests:
#  #        pp_application: "puppet"
#  #        pp_role: "pe_xl::primary_master"
#  #        pp_cluster: "A"
#  #      | HEREDOC
#  #  )
#  #
#  #  run_task('pe_xl::mkdir_p_file', $puppetdb_database_host,
#  #    path    => '/etc/puppetlabs/puppet/csr_attributes.yaml',
#  #    content => @("HEREDOC"),
#  #      ---
#  #      extension_requests:
#  #        pp_application: "puppet"
#  #        pp_role: "pe_xl::puppetdb_database"
#  #        pp_cluster: "A"
#  #      | HEREDOC
#  #  )
#  #
#  #  run_task('pe_xl::mkdir_p_file', $puppetdb_database_replica_host,
#  #    path    => '/etc/puppetlabs/puppet/csr_attributes.yaml',
#  #    content => @("HEREDOC"),
#  #      ---
#  #      extension_requests:
#  #        pp_application: "puppet"
#  #        pp_role: "pe_xl::puppetdb_database"
#  #        pp_cluster: "B"
#  #      | HEREDOC
#  #  )
#  #
#  #  # Get the primary master installation up and running. The installer will
#  #  # "fail" because PuppetDB can't start. That's expected.
#  #  without_default_logging() || {
#  #    notice("Starting: task pe_xl::pe_install on ${primary_master_host}")
#  #    run_task('pe_xl::pe_install', $primary_master_host,
#  #      _catch_errors         => true,
#  #      tarball               => $upload_tarball_path,
#  #      peconf                => '/tmp/pe.conf',
#  #      shortcircuit_puppetdb => true,
#  #    )
#  #    notice("Finished: task pe_xl::pe_install on ${primary_master_host}")
#  #  }
#  #
#  #  # Configure autosigning for the puppetdb database hosts 'cause they need it
#  #  run_task('pe_xl::mkdir_p_file', $primary_master_host,
#  #    path    => '/etc/puppetlabs/puppet/autosign.conf',
#  #    owner   => 'pe-puppet',
#  #    group   => 'pe-puppet',
#  #    mode    => '0644',
#  #    content => @("HEREDOC"),
#  #      $puppetdb_database_host
#  #      $puppetdb_database_replica_host
#  #      | HEREDOC
#  #  )
#  #
#  #  # Run the PE installer on the puppetdb database hosts
#  #  run_task('pe_xl::pe_install', [$puppetdb_database_host, $puppetdb_database_replica_host],
#  #    tarball => $upload_tarball_path,
#  #    peconf  => '/tmp/pe.conf',
#  #  )
#  #
#  #  # Now that the main PuppetDB database node is ready, finish priming the
#  #  # primary master
#  #  run_command('systemctl start pe-puppetdb', $primary_master_host)
#  #  run_task('pe_xl::rbac_token', $primary_master_host,
#  #    password => $console_password,
#  #  )
#  #  run_task('pe_xl::code_manager', $primary_master_host,
#  #    action => 'file-sync commit',
#  #  )
#  #
#  #  # Deploy the PE agent to all remaining hosts
#  #  run_task('pe_xl::agent_install', $primary_master_replica_host,
#  #    server        => $primary_master_host,
#  #    install_flags => [
#  #      ' --puppet-service-ensure ', 'stopped',
#  #      " main:dns_alt_names=${dns_alt_names_csv}",
#  #      ' extension_requests:pp_application=puppet',
#  #      ' extension_requests:pp_role=pe_xl::primary_master',
#  #      ' extension_requests:pp_cluster=B',
#  #    ],
#  #  )
#  #
#  #  # TODO: Split the compile masters into two pools, A and B.
#  #  run_task('pe_xl::agent_install', $compile_master_hosts,
#  #    server        => $primary_master_host,
#  #    install_flags => [
#  #      '--puppet-service-ensure', 'stopped',
#  #      "main:dns_alt_names=${dns_alt_names_csv}",

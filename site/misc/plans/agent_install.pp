plan misc::agent_install (
) {

  # TODO: Split the compile masters into two pools, A and B.
  run_task('pe_xl::agent_install', $compile_master_hosts,
    server        => $primary_master_host,
    install_flags => [
      ' --puppet-service-ensure ', 'stopped',
      ' extension_requests:pp_application=agent',
      ' extension_requests:pp_role=pe_xl::agent',
    ],
  )

  run_task('pe_xl::puppet_runonce', $agent_installer_hosts)

  return('Agent Installation succeeded')
}

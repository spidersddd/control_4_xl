plan misc::puppet_in_order (
  String[1]           $version = '2018.1.2',
  String[1]           $console_password,
  Hash                $r10k_sources = { },

  String[1]           $primary_master_host,
  String[1]           $puppetdb_database_host,
  Array[String[1]]    $compile_master_hosts = [ ],
  Array[String[1]]    $dns_alt_names = [ ],

  Optional[String[1]] $primary_master_replica_host = undef,
  Optional[String[1]] $puppetdb_database_replica_host = undef,

  Optional[String[1]] $load_balancer_host = undef,

  String[1]           $stagingdir = '/tmp',
) {


  run_task('pe_xl::puppet_runonce', $primary_master_host)
  run_task('pe_xl::puppet_runonce', $puppetdb_database_host)
  run_task('pe_xl::puppet_runonce', $primary_master_replica_host)
  run_task('pe_xl::puppet_runonce', $puppetdb_database_replica_host)
  $compile_master_hosts.each |$host| {
    run_task('pe_xl::puppet_runonce', $host)
  }
  if $load_balancer_host {
    run_task('pe_xl::puppet_runonce', $load_balancer_host)
  }

  return('Puppet Runs Completed')
}

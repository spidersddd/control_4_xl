class profile::haproxy {

  class { 'haproxy':
    global_options   => {
      'log'     => "${facts['ipaddress']} local2",
      'chroot'  => '/var/lib/haproxy',
      'pidfile' => '/var/run/haproxy.pid',
      'maxconn' => 5000,
      'user'    => 'haproxy',
      'group'   => 'haproxy',
      'daemon'  => '',
      'stats'   => 'socket /var/lib/haproxy/stats',
    },
    defaults_options => {
      'timeout' => [
        'connect 10s',
        'queue 1m',
        'client 2m',
        'server 2m',
        'http-request 120s',
      ]
    }
  }

  haproxy::listen { 'puppetdb':
    collect_exported => true,
    mode             => 'tcp',
    ipaddress        => $facts['ipaddress'],
    ports            => '8081',
    options          => {
      option  => ['tcplog'],
      balance => 'leastconn',
    },
  }

  haproxy::listen { 'puppetserver':
    collect_exported => true,
    mode             => 'tcp',
    ipaddress        => $facts['ipaddress'],
    ports            => '8140',
    options          => {
      option  => ['tcplog'],
      balance => 'leastconn',
    },
  }

  haproxy::listen { 'pcp-broker':
    collect_exported => true,
    mode             => 'tcp',
    ipaddress        => $facts['ipaddress'],
    ports            => '8142',
    options          => {
      option  => ['tcplog'],
      balance => 'roundrobin',
      timeout => [
        'tunnel 15m',
      ],
    },
  }

  haproxy::listen { 'stats':
    ipaddress        => '*',
    ports            => '9090',
    mode             => 'http',
    options          => { 'stats' => ['uri /', 'auth puppet:puppet', 'refresh 3s'] },
    collect_exported => false,
  }

  [8140, 8142, 9090, 8081].each |$port| {
    selinux::port { "Allow port ${port}":
      seltype  => 'http_port_t',
      port     => $port,
      protocol => 'tcp',
      notify   => Service['haproxy'],
    }
  }

}

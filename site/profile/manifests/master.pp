class profile::master (
  Boolean $cleanup = false,
) {

  if $cleanup {
    file { '/etc/puppetlabs/mcollective':
      ensure => absent,
      force  => true,
    }

    pe_hocon_setting { 'Redact unused console_admin_password in pe.conf':
      ensure  => present,
      path    => '/etc/puppetlabs/enterprise/conf.d/pe.conf',
      setting => 'console_admin_password',
      value   => '*** REDACTED ***',
    }

    pe_hocon_setting { 'Redact unused password in orchestrator.conf':
      ensure  => present,
      path    => '/etc/puppetlabs/orchestration-services/conf.d/orchestrator.conf',
      setting => 'orchestrator.database.password',
      value   => '*** REDACTED ***',
    }
  }
}

class profile::compile_master {

  @@haproxy::balancermember { "puppetserver-${facts['hostname']}":
    listening_service => 'puppetserver',
    ports             => '8140',
    server_names      => $facts['hostname'],
    ipaddresses       => $facts['ipaddress'],
    options           => 'check',
  }

  @@haproxy::balancermember { "pcp-broker-${facts['hostname']}":
    listening_service => 'pcp-broker',
    ports             => '8142',
    server_names      => $facts['hostname'],
    ipaddresses       => $facts['ipaddress'],
    options           => 'check',
  }

  @@haproxy::balancermember { "puppetdb-${facts['hostname']}":
    listening_service => 'puppetdb',
    ports             => '8081',
    server_names      => $facts['hostname'],
    ipaddresses       => $facts['ipaddress'],
    options           => 'check',
  }

}
